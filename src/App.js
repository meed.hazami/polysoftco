import "./App.css";
import { Navbar } from "./components/navbar";
import Home from "./components/body/index"

function App() {
  return (
    <div className="App">
      <Navbar />
      <Home/>
    </div>
  );
}

export default App;
