import React, { useState, useEffect } from "react";
import boxAvatar1 from "../../assets/images/03-ValProp1.jpg";
import boxAvatar2 from "../../assets/images/03-ValProp2.jpg";
import boxAvatar3 from "../../assets/images/03-ValProp3.jpg";
import DemandeDemo from "./demandeDemo";

export default function Section4(props) {
  const [isOpen, setOpen] = useState(false);
  const [isOpen1, setOpen1] = useState(false);
  const [isOpen2, setOpen2] = useState(false);

  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
    setOpen(position >= 583);
    setOpen1(position >= 800);
    setOpen2(position >= 900);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll, { passive: true });

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <div>
      {/* {console.log("scrollPosition", scrollPosition)} */}
      <section style={{ maxWidth: "80%", margin: "auto" }}>
        <h1
          style={{
            marginTop: "100px",
            color: "#121118",
            fontWeight: "700",
            letterSpacing: "normal",
            fontSize: "2.41176rem",
            lineHeight: "2.94118rem",
          }}
        >
          A propos:
        </h1>
        <p
          style={{
            color: "rgb(93, 93, 105)",
            marginTop: "0.58824rem",
            fontWeight: "400",
            letterSpacing: "normal",
            fontSize: "1.41176rem",
            lineHeight: "1.94118rem",
          }}
        >
          Une Start Up Tunisienne basée à Sfax créée en Février 2019 spécialisée
          dans le développement de solutions informatiques, issue du groupe
          familial opérant dans le secteur informatique depuis 1988, Nous
          croyons que chacun de nos produits doit évoluer et s'adapter aux
          besoins du marché afin de garantir une meilleure satisfaction client.
          Toujours soucieux de garantir le meilleur, nos efforts ne cesseront
          jamais d'évoluer graçe à un processus de développement technique et
          commercial complet et sur mesure.
        </p>
        <hr
          style={{
            border: "solid #e1e6eb",
            borderWidth: "1px 0 0",
            clear: "both",
            margin: "1.5rem 0 1.44118rem",
            width: "80%",
            marginTop: "50px",
            marginBottom: "50px",
          }}
        ></hr>
      </section>

      <div
        id="demarrer"
        style={{
          marginTop: "50px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          width: "100%",
          backgroundColor: "#121118",
          paddingBottom: "2.94118rem",
        }}
      >
        <h2 style={{ marginTop: "50px", color: "#fff" }}>
          Démarrer votre affaire avec 24-CRM
        </h2>
      </div>

      <section
        className={isOpen ? "section11" : ""}
        style={{
          marginTop: "50px",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          flexWrap: "wrap",
        }}
      >
        <img
          style={{ width: "300px", heigth: "auto" }}
          src={boxAvatar1}
          alt="React Logo"
        />
        <div className="paragraphhome">
          <h1
            style={{
              textAlign: "start",
              fontWeight: "700",
              letterSpacing: "normal",
              fontSize: "2rem",
              lineHeight: "2.64706rem",
            }}
          >
            Notre Vision
          </h1>
          <ul>
            <li
              style={{
                textAlign: "start",
                lineHeight: "2rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              Notre vision et d'être un leader du secteur sur le marché local,
              nos ambitions dépassent les frontières nous visons à
              s'internationaliser avec des partenaires ciblés notammant sur
              l'Afrique
            </li>
          </ul>
        </div>
      </section>

      <section
        className={isOpen1 ? "section12" : ""}
        style={{
          marginTop: "50px",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          flexWrap: "wrap",
          backgroundColor: "#EDEFF3",
        }}
      >
        <div className="paragraphhome">
          <h1
            style={{
              textAlign: "start",
              fontWeight: "700",
              letterSpacing: "normal",
              fontSize: "2rem",
              lineHeight: "2.64706rem",
              marginTop: "20px",
            }}
          >
            Notre Mission
          </h1>
          <ul>
            <li
              style={{
                textAlign: "start",
                lineHeight: "2rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              Notre mission est de créer des solutions de gestion performantes
              et innovantes pour les PME tous secteurs confondus ainsi les
              cabinets comptables.
            </li>
            <li
              style={{
                textAlign: "start",
                lineHeight: "2rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              Nous adaptons nos logiciels aux besoins les plus specifiques du
              client et nous offrons dessolutions sur mesure
            </li>
          </ul>
        </div>
        <img
          style={{ width: "300px", heigth: "auto", marginBottom: "20px" }}
          src={boxAvatar2}
          alt="React Logo"
        />
      </section>

      <section
        className={isOpen2 ? "section11" : ""}
        style={{
          marginTop: "50px",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          flexWrap: "wrap",
        }}
      >
        <img
          style={{ width: "300px", heigth: "auto" }}
          src={boxAvatar3}
          alt="React Logo"
        />
        <div className="paragraphhome">
          <h1
            style={{
              textAlign: "start",
              fontWeight: "700",
              letterSpacing: "normal",
              fontSize: "2rem",
              lineHeight: "2.64706rem",
            }}
          >
            Nos valeurs
          </h1>
          <ul>
            <li
              style={{
                textAlign: "start",
                lineHeight: "2rem",
                color: "rgb(93, 93, 105)",
              }}
            >
              INNOVATION , SATISFACTION CLIEN , ENGAGEMENT , RESPONSABILITE
            </li>
          </ul>
        </div>
      </section>

      <hr
        style={{
          border: "solid #e1e6eb",
          borderWidth: "1px 0 0",
          clear: "both",
          margin: "1.5rem 0 1.44118rem",
          width: "80%",
          margin: "auto",
          marginTop: "50px",
          marginBottom: "50px",
        }}
      ></hr>

      <div
        id="NotreCRM"
        style={{
          marginTop: "50px",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          width: "100%",
          backgroundColor: "#121118",
          paddingBottom: "2.94118rem",
        }}
      >
        <h2 style={{ marginTop: "50px", color: "#fff" }}>Notre CRM</h2>
      </div>

      <section style={{ backgroundColor: "#EDEFF3", width: "100%" }}>
        <div
          style={{
            width: "80%",
            margin: "auto",
            paddingTop: "50px",
            paddingBottom: "50px",
          }}
        >
          <div className="video-responsive">
            <iframe
              width="853"
              height="480"
              src={`https://www.youtube.com/embed/fcsLZhbKLTY?autoplay=1&mute=1`}
              frameBorder="0"
              allow="accelerometer; clipboard-write; autoplay; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
              title="Embedded youtube"
            />
          </div>
        </div>
      </section>

      <section id="DemandeDemo">
        <DemandeDemo />
      </section>
    </div>
  );
}
