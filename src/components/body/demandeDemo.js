import React from "react";
import Button from "@mui/material/Button";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import emailjs from "@emailjs/browser";
import TextField from "@mui/material/TextField";

export default class DemandeDemo extends React.Component {
  state = {
    formData: {
      email: "",
    },
    submitted: "Envoyer",
  };

  handleChange = (event) => {
    const { formData } = this.state;
    formData[event.target.name] = event.target.value;
    this.setState({ formData });
  };

  // "XR829Ehk83JuetQGz"
  // e.target,

  handleSubmit = (e) => {
    e.preventDefault();
    console.log("ok");
    emailjs
      .sendForm(
        "service_POLYSOFT",
        "template_9ze70ce",
        e.target,
        "XR829Ehk83JuetQGz"
      )
      .then(
        (result) => {
          this.setState({ submitted: "creation avec succès" });
        },
        (error) => {
          console.log("no", error.text);
        }
      );
    e.target.reset();
  };

  render() {
    const { formData, submitted } = this.state;
    return (
      <>
        <div
          id="vertion Demo"
          style={{
            marginTop: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            width: "100%",
            backgroundColor: "#121118",
            paddingBottom: "2.94118rem",
          }}
        >
          <h2 style={{ marginTop: "50px", color: "#fff" }}>
            Demander une vertion Demo
          </h2>
        </div>
        <ValidatorForm
          style={{
            height: "400px",
            width: "50%",
            margin: "auto",
          }}
          ref="form"
          onSubmit={this.handleSubmit}
        >
          <div style={{ height: "40%", margin: "auto" }}></div>
          <section
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-around",
              flexWrap: "wrap",
            }}
          >
            <TextValidator
              style={{ width: "300px" }}
              size="medium"
              label="Email"
              onChange={this.handleChange}
              name="email"
              value={formData.email}
              validators={["required", "isEmail"]}
              errorMessages={["this field is required", "email is not valid"]}
            />
            <div style={{ display: "none" }}>
              <TextField
                defaultValue={"PSDEMO" + parseInt(Math.random() * 100)}
                name="code"
              />
              <TextField
                defaultValue={"Demo" + parseInt(Math.random() * 100)}
                name="nom"
              />
              <TextField
                defaultValue={parseInt(Math.random() * 10000)}
                name="mp"
              />
            </div>

            <Button
              size="large"
              color="primary"
              variant="contained"
              type="submit"
              value="Send"
              disabled={this.state.submitted === "creation avec succès"}
            >
              {this.state.submitted}
            </Button>
          </section>
        </ValidatorForm>
      </>
    );
  }
}
