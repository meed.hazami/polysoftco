import React, { Component } from 'react'
import Section1 from "./section1"
import Section4 from "./section4"
import Section5 from "./section5"



export default class Home extends Component {
  render() {
    return (
      <>
      <Section1/>
      <Section4/>
      <Section5/>
      </>
    )
  }
}
