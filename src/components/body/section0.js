import React from 'react'
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import AddIcCallIcon from '@mui/icons-material/AddIcCall';


export default function Section0() {
    return (
      <div style={{
          paddingTop: "50px",
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
      }}>
          <span style={{
              marginLeft: "4%"
          }}>
               <h1 style ={{color : "#121118"}}>Shop now</h1>
          </span>
          <span style={{
              marginRight: "4%"
          }}>
             <FacebookIcon style ={{color : "#121118" , marginRight: "7px"}}/>
             <TwitterIcon style ={{color : "#121118" , marginRight: "7px"}}/>
             <InstagramIcon style ={{color : "#121118" , marginRight: "7px"}}/>
             <AddIcCallIcon style ={{color : "#121118" , marginRight: "7px"}}/>
          </span>
      </div>
    )
}

