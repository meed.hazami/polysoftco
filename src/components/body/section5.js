import React, { Component } from "react";
import FacebookIcon from "@mui/icons-material/Facebook";
import AddIcCallIcon from "@mui/icons-material/AddIcCall";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import EmailIcon from "@mui/icons-material/Email";

export default class Section5 extends Component {
  render() {
    return (
      <section
        style={{
          backgroundColor: "#2B2935",
          width: "100%",
          minHeight: "400px",
        }}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
            width: "100%",
            flexWrap: "wrap",
            minHeight: "300px",
          }}
        >

          <section
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "start",
              height: "100px",
              marginTop : "50px"
            }}
          >
            <h2 style={{ color: "rgb(25, 118, 210)" }}>POLYSOFT</h2>
            <h2 style={{ color: "rgb(226, 127, 81)" }}>&amp;CO</h2>
          </section>

          <div style={{ height: "20px" , height: "100px" , marginTop : "50px"}}>
            <h3
              style={{
                color: "#fff",
              }}
            >
              Contactez-nous : 
            </h3>
            <ul style={{ textAlign: "start", listStyleType: "none" }}>
              <li style={{ color: "#fff", paddingLeft: "50px" }}>
              <a style={{ color: "#fff" }} href="tel:+21674443620">
                  <AddIcCallIcon /> 216 74 443 620
              </a>
              </li>
              <li style={{ color: "#fff", paddingLeft: "50px" }}>
              <a style={{ color: "#fff" }} href="tel:+21653416577">
                  <AddIcCallIcon /> 216 53 416 577
              </a>
              </li>
              <li style={{ color: "#fff", paddingLeft: "50px" }}>

                <a style={{ color: "#fff" }} href="mailto:gm.polysoftco@gmail.com">
                  <EmailIcon />
                  gm.polysoftco@gmail.com
                </a>

              </li>
            </ul>
          </div>

          <div style={{ height: "20px" , height: "100px" , marginTop : "50px"}}>
            <h3
              style={{
                color: "#fff",
              }}
            >
              Réseaux sociaux :
            </h3>
            <ul style={{ textAlign: "start", listStyleType: "none" }}>
              <li style={{ color: "#fff"}}>
                <a style={{ color: "#fff" }} target="_blank" href="https://www.facebook.com/polysoftco/">
                  <FacebookIcon />
                  facebook
                </a>
              </li>
              <li style={{ color: "#fff" }} >
                <a style={{ color: "#fff" }} target="_blank" href="https://www.linkedin.com/company/polysoft-co/">
                  <LinkedInIcon />
                  linkedin
                </a>
              </li>
            </ul>
          </div>
        </div>
        <h8 style={{color : "#fff"}}>SITE WEB RÉALISÉ PAR 
            <h8 style={{ color: "rgb(25, 118, 210)" }}>  POLYSOFT</h8>
            <h8 style={{ color: "rgb(226, 127, 81)" }}>&amp;CO </h8>
        | © 2022. TOUS DROITS RÉSERVÉS</h8>
      </section>
    );
  }
}
