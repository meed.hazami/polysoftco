import React, { Component } from "react";
import etap1 from "../../assets/images/etap1.webp";
import etap2 from "../../assets/images/etap2.webp";
import etap3 from "../../assets/images/etap3.webp";

export default class Section2 extends Component {
  render() {
    return (
      <div
        style={{
          marginTop: "100px",
        }}
      >
        <h1 style={{marginBottom : "50px" , color: "#121118" }}>
          The simple way to tackle the complexity of selling online
        </h1>
        <section style={{display: "flex" , justifyContent: "space-around" , flexWrap : "wrap"}}>
          <span style={{ maxWidth : "350px" ,marginTop : "50px"}}>
            <img
              style={{ maxHeight: "3.23529rem" }}
              src={etap1}
              alt="React Logo"
            />
            <h1>Start a trial</h1>
            <p style={{textAlign: "start" , marginTop: "0.58824rem" , color: "#5D5D69"}}>Build a store that caters to your unique business needs—with the design tools, features, and support to get you up and running.</p>
          </span>
          <span style={{ maxWidth : "350px" ,marginTop : "50px"}}>
            <img
             style={{ maxHeight: "3.23529rem" }}
             src={etap2} alt="React Logo" />
            <h1>Launch your store</h1>
            <p style={{textAlign: "start" , marginTop: "0.58824rem" , color: "#5D5D69"}}>Design and customize a beautiful storefront with powerful features and expert ecommerce support along the way.</p>
          </span>
          <span style={{ maxWidth : "350px" ,marginTop : "50px"}}>
            <img
            style={{ maxHeight: "3.23529rem" }}
            src={etap3} alt="React Logo" />
            <h1>Grow your business</h1>
            <p style={{textAlign: "start" , marginTop: "0.58824rem" , color: "#5D5D69"}}>Build a store that caters to your unique business needs—with the design tools, features, and support to get you up and running.</p>
          </span>
        </section>
      </div>
    );
  }
}
