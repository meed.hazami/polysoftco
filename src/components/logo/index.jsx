import React from "react";
import styled from "styled-components";
import GreenlandLogoImg from "../../assets/images/logo.PNG";

const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const LogoImg = styled.div`
  width: 29px;
  height: 29px;

  img {
    width: 100%;
    height: 100%;
  }
`;

const LogoText = styled.h2`
  font-size: 16px;
  margin: 0;
  margin-left: 4px;
  color: #222;
  font-weight: 500;
`;

export function Logo(props) {
  return (
    <section style={{display : "flex" , justifyContent:"center" , alignItems : "center"}}>
       <h5 style={{color: "rgb(25, 118, 210)"}}>POLYSOFT</h5>
       <h5 style={{color: "rgb(226, 127, 81)"}}>&amp;CO</h5>
    </section>
  );
}
