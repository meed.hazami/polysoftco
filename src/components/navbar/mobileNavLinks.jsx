import React, { useState ,useEffect  } from "react";
import styled from "styled-components";
import { MenuToggle } from "./menuToggle";

const NavLinksContainer = styled.div`
  display: flex;
  align-items: center;
  
`;

const LinksWrapper = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  height: auto;
  list-style: none;
  background-color: #fff;
  width: 100%;
  flex-direction: column;
  position: fixed;
  top: 65px;
  left: 0;
`;

const LinkItem = styled.li`
  width: 100%;
  padding: 0 1.1em;
  color: #222;
  font-weight: 500;
  font-size: 16px;

  margin-bottom: 10px;
`;

const Link = styled.a`
  text-decoration: none;
  color: inherit;
  font-size: inherit;
`;

const Marginer = styled.div`
  height: 2em;
`;

export function MobileNavLinks(props) {
  const [isOpen, setOpen] = useState(false);

  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.pageYOffset;
    setScrollPosition(position);
    setOpen(position === 0 && isOpen )
};

useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
    
    return () => {
        window.removeEventListener('scroll', handleScroll);
    };
}, []);

  return (
    <NavLinksContainer style={{zIndex : "99999999" }}>
      <MenuToggle  isOpen={isOpen} toggle={() => setOpen(!isOpen)} />
      {isOpen && (
        <LinksWrapper style={{ marginTop: "2px"}}>
          <LinkItem style={{padding: "2%"}}>
              <a className="tran1" style={{ textDecorationLine: "none"}} href="#">Acceuil</a>
          </LinkItem>
          <LinkItem style={{padding: "2%"}}>
              <a className="tran2" style={{ textDecorationLine: "none"}} href="#NotreCRM">Nos Produits</a>
          </LinkItem>
          <LinkItem style={{paddingTop: "2%"}}>
            <a className="tran3" style={{ textDecorationLine: "none"}} href="#demarrer">Nos Services</a>
          </LinkItem>
          <LinkItem style={{paddingTop: "2%"}}>
            <a className="tran3" style={{ textDecorationLine: "none"}} href="#DemandeDemo">Version demo</a>
          </LinkItem>
          <Marginer />
        </LinksWrapper>
      )}
    </NavLinksContainer>
  );
}



