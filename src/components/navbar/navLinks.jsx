import React from "react";
import styled from "styled-components";

const NavLinksContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
`;

const LinksWrapper = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  height: 100%;
  list-style: none;
`;

const LinkItem = styled.li`
  height: 100%;
  padding: 0 1.1em;
  color: #2367fa;
  font-weight: 500;
  font-size: 14px;
  align-items: center;
  justify-content: center;
  display: flex;
  border-top: 2px solid transparent;
  transition: all 220ms ease-in-out;

  &:hover {
    border-top: 2px solid #133c99;
  }
`;

const Link = styled.a`
  text-decoration: none;
  color: inherit;
  font-size: inherit;
`;

export function NavLinks(props) {
  return (
    <NavLinksContainer>
      <LinksWrapper >
        <div style={{ width : "700px", 
                     display : "flex" ,
                     justifyContent : "space-around" , 
                     alignItems :  "center" }}>
          <h5>
            <a style={{ textDecorationLine: "none"}} href="#">Acceuil</a>
          </h5>
          <h5>
            <a style={{ textDecorationLine: "none"}} href="#NotreCRM">Nos Produits</a>
          </h5>
          <h5>
            <a style={{ textDecorationLine: "none"}} href="#demarrer">Nos Services</a>
          </h5>
          <h5>
            <a style={{ textDecorationLine: "none"}} href="#DemandeDemo">Version demo</a>
          </h5>
        </div>
      </LinksWrapper>
    </NavLinksContainer>
  );
}
